FROM python:3.7.10

COPY ./ ./
RUN echo "### install starts ###"
RUN pip install -r requirements.txt
RUN echo "### install ends ###"
WORKDIR /
EXPOSE 8050
CMD ["python3", "./app.py"]