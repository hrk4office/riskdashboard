# Risk Dashboard from CJ ENM COMMERCE 
from dash import Dash, dcc, html, Input, Output
import pandas as pd
import plotly.express as px

app = Dash(__name__, title="Risk Dashboard")

# -- Get and clean data (reading excel into pandas)
df = pd.read_excel('risk_report.xlsx', engine='openpyxl')

risk_tot_cnt = df.groupby(['Part'])[['Cnt']].sum()
risk_tot_cnt['Total'] = df.groupby(['Part'])[['Part']].count()

risk_tot_cnt['SolveRatio'] = round(((risk_tot_cnt['Total'] - risk_tot_cnt['Cnt']) / risk_tot_cnt['Total']) * 100)

risk_tot_cnt['AccpLT'] = df.groupby(['Part'])[['AccpLT']].mean()
risk_tot_cnt['SolveLT'] = df.groupby(['Part'])[['SolveLT']].mean()
risk_tot_cnt['CloseLT'] = df.groupby(['Part'])[['CloseLT']].mean()

risk_tot_cnt['UI'] = df[df['Category'] == "UI"].groupby(['Part'])[['Category']].count()
risk_tot_cnt['TR'] = df[df['Category'] == "TR"].groupby(['Part'])[['Category']].count()
risk_tot_cnt['BIZ'] = df[df['Category'] == "BIZ"].groupby(['Part'])[['Category']].count()

risk_tot_cnt.fillna(0, inplace=True)
risk_tot_cnt = risk_tot_cnt.astype(int)

# -- Figure
# Defect Ratio Graph
barFig = px.bar(risk_tot_cnt, x='SolveRatio', y=risk_tot_cnt.index, title='<i>Stage of Completion</i>', labels={'SolveRatio':'rate'}, hover_name=risk_tot_cnt.index,
                color=risk_tot_cnt.index, color_discrete_sequence=['#ffb801','#2cc0c9','#db765f','#a167c6'], text=[f'{i}%' for i in risk_tot_cnt['SolveRatio']],
                height=330, category_orders={'Part':['상품','주문','채널','물류']})
barFig.update_layout(plot_bgcolor='rgb(46,56,88)', paper_bgcolor='rgb(46,56,88)', font_color='rgb(255,255,255)', title_x=0.5)
barFig.update_traces(insidetextfont={'color':'rgb(0,0,0)'})
barFig.update_xaxes(range=(0,100))
                    
# Defect Types Graph
uiFig = px.bar(risk_tot_cnt, y='UI', x=risk_tot_cnt.index, orientation='v', title='<b>UI</b>', height=350, hover_name=risk_tot_cnt.index,
               color=risk_tot_cnt.index, color_discrete_sequence=['#ffb801','#2cc0c9','#db765f','#a167c6'],
               category_orders={'Part':['상품','주문','채널','물류']})
bizFig = px.bar(risk_tot_cnt, y='BIZ', x=risk_tot_cnt.index, orientation='v', title='<b>Biz Logic</b>', height=350, hover_name=risk_tot_cnt.index,
                color=risk_tot_cnt.index, color_discrete_sequence=['#ffb801','#2cc0c9','#db765f','#a167c6'],
                category_orders={'Part':['상품','주문','채널','물류']})
trFig = px.bar(risk_tot_cnt, y='TR', x=risk_tot_cnt.index, orientation='v', title='<b>Transaction</b>', height=350, hover_name=risk_tot_cnt.index,
               color=risk_tot_cnt.index, color_discrete_sequence=['#ffb801','#2cc0c9','#db765f','#a167c6'],
               category_orders={'Part':['상품','주문','채널','물류']})

uiFig.update_layout({'plot_bgcolor':'rgb(46,56,88)', 'paper_bgcolor':'rgb(46,56,88)', 'font_color':'rgb(255,255,255)', 'title_x' : 0.5, 'title_font_size':20})
uiFig.update_traces(width=0.7, showlegend=False)
uiFig.update_xaxes(tickfont_size=17, ticks="outside", ticklen=20, tickwidth=10)
bizFig.update_layout({'plot_bgcolor':'rgb(46,56,88)', 'paper_bgcolor':'rgb(46,56,88)', 'font_color':'rgb(255,255,255)', 'title_x' : 0.5, 'title_font_size':20})
bizFig.update_traces(width=0.7, showlegend=False)
bizFig.update_xaxes(tickfont_size=17, ticks="outside", ticklen=20, tickwidth=10)
trFig.update_layout({'plot_bgcolor':'rgb(46,56,88)', 'paper_bgcolor':'rgb(46,56,88)', 'font_color':'rgb(255,255,255)', 'title_x' : 0.5, 'title_font_size':20})
trFig.update_traces(width=0.7, showlegend=False)
trFig.update_xaxes(tickfont_size=17, ticks="outside", ticklen=20, tickwidth=10)

# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div(style={'width':'100%', 'height':'100%'}, children=[
    # Title
    html.H1(style={'fontWeight':'bold', 'fontSize':50, 'color':'#ffffff', 'text-align':'left', 'marginBottom':0, 'marginLeft':'20px'},
            children='RISK DASHBOARD'),
    html.H6(style={'fontWeight':'normal', 'fontSize':25, 'color':'#ffffff', 'text-align':'left', 'margin':0, 'marginBottom':'30px', 'marginLeft':'40px'},
            children=['CJENM Commerce Renewal Project']),
    
    # Card Section
    html.Div(
        style={'display':'flex', 'justify-content': 'space-evenly'},
        children=[
            html.Div(
                className='card-style card1-image',
                children=[
                    html.P(className='card-title', children='Products'),
                    html.H1(className='card-cnt', children='{a}'.format(a=risk_tot_cnt.loc['상품']['Cnt']))
                ]
            ),
            html.Div(
                className='card-style card2-image',
                children=[
                    html.P(className='card-title', children='Orders'),
                    html.H1(className='card-cnt', children='{a}'.format(a=risk_tot_cnt.loc['주문']['Cnt']))
                ]
            ),
            html.Div(
                className='card-style card3-image',
                children=[
                    html.P(className='card-title', children='Channel'),
                    html.H1(className='card-cnt', children='{a}'.format(a=risk_tot_cnt.loc['채널']['Cnt']))
                ]
            ),
            html.Div(
                className='card-style card4-image',
                children=[
                    html.P(className='card-title', children='Fulfillment'),
                    html.H1(className='card-cnt', children='{a}'.format(a=risk_tot_cnt.loc['물류']['Cnt']))
                ]
            )
        ]
    ),
    
    # Graph 1 Section
    html.Div(
        style={'display':'flex', 'justify-content': 'space-around'},
        children=[
            # Graph
            html.Div(
                style={'width':'32%'},
                children=[
                    html.P(className='title', children='결함 해결 진척률'),
                    html.Div(children=[dcc.Graph(figure=barFig)])
                ]
            ),
            # Text Box
            html.Div(
                style={'width':'60%'},
                children=[
                    html.P(className='title', children='결함 처리 소요시간'),
                    html.Div(
                        style={'display':'flex', 'flex-direction':'row', 'justify-content': 'space-between', 'align-items': 'stretch', 'color':'#ffffff'},
                        children=[
                            html.Div(
                                className='text-sec-style',
                                children=[
                                    html.P(className='text-title', children='상품파트'),
                                    html.Div(
                                        className='text-body-style',
                                        children=[
                                            html.Div(
                                                children=[
                                                    html.Span('접수 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['상품']['AccpLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('처리 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['상품']['SolveLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('종료 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['상품']['CloseLT'])),
                                                    html.Span('　일')
                                                ]
                                            )
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className='text-sec-style',
                                children=[
                                    html.P(className='text-title', children='주문파트'),
                                    html.Div(
                                        className='text-body-style',
                                        children=[
                                            html.Div(
                                                children=[
                                                    html.Span('접수 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['주문']['AccpLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('처리 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['주문']['SolveLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('종료 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['주문']['CloseLT'])),
                                                    html.Span('　일')
                                                ]
                                            )
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className='text-sec-style',
                                children=[
                                    html.P(className='text-title', children='채널파트'),
                                    html.Div(
                                        className='text-body-style',
                                        children=[
                                            html.Div(
                                                children=[
                                                    html.Span('접수 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['채널']['AccpLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('처리 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['채널']['SolveLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('종료 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['채널']['CloseLT'])),
                                                    html.Span('　일')
                                                ]
                                            )
                                        ]
                                    )
                                ]
                            ),
                            html.Div(
                                className='text-sec-style',
                                children=[
                                    html.P(className='text-title', children='물류파트'),
                                    html.Div(
                                        className='text-body-style',
                                        children=[
                                            html.Div(
                                                children=[
                                                    html.Span('접수 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['물류']['AccpLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('처리 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['물류']['SolveLT'])),
                                                    html.Span('　일')
                                                ]
                                            ),
                                            html.Div(
                                                children=[
                                                    html.Span('종료 소요 시간　　'),
                                                    html.Span(className='text-body-font', children='{a}'.format(a=risk_tot_cnt.loc['물류']['CloseLT'])),
                                                    html.Span('　일')
                                                ]
                                            )
                                        ]
                                    )
                                ]
                            )
                        ]
                    )
                ]
            )
        ]
    ),
    
    # Graph 2 Section
    html.P(className='title', style={'margin-left':'2%'}, children='유형별 결함 건수'),
    html.Div(
        style={'display':'flex', 'flex-flow':'row', 'justify-content': 'space-around', 'marginBottom':'2px', 'marginLeft':'0.5%', 'marginRight':'0.5%'},
        children=[
            html.Div(
                style={'width':'30%'},
                children=[
                    html.Div(children=[dcc.Graph(figure=uiFig)])
                ]
            ),
            html.Div(
                style={'width':'30%'},
                children=[
                    html.Div(children=[dcc.Graph(figure=bizFig)])
                ]
            ),
            html.Div(
                style={'width':'30%'},
                children=[
                    html.Div(children=[dcc.Graph(figure=trFig)])
                ]
            )
        ]
    )
])

# ------------------------------------------------------------------------------
if __name__ == '__main__':
    app.run_server(host='0.0.0.0', port=8050, debug=True)